
var data1 = {
  type: 0,
};

var callback1 = function(response) {

  var response = $.parseJSON(response)

  $("#input_1").autocomplete({
    source: response,
    minLength: 3
  });

  $("#input_2").autocomplete({
    source: response,
    minLength: 3
  });
};

$.get('/index.php', data1, callback1)


function sendAJAX() {

  $("fieldset").attr("disabled", true)
  $("#movie_results").empty()

  var first_actor = $("input[name=firstactor]").val()
  var second_actor = $("input[name=secondactor]").val()

  if (first_actor === '') {
    first_actor = 'Nick Frost'
  }

  if (second_actor == "")
    second_actor = "Simon Pegg"

  var data = {
    type: 1,
    firstactor: first_actor,
    secondactor: second_actor
  };

  var callback = function(response) {

  //  console.log(response)
    var response = $.parseJSON(response)
    $("#gallery").html(response["gallery"])
    $("#spotlight").html(response["spotlight"])

    $("fieldset").attr("disabled", false)
  };

  $.get('/index.php', data, callback)
};
