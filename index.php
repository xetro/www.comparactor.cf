<?php

require 'functions.php';
require __DIR__ . '/vendor/autoload.php';

ini_set('display_errors', 1);

if ($_REQUEST["type"] == 1) {

  $person_1_obj = searchPersonQuery($_REQUEST['firstactor']);
  $person_2_obj = searchPersonQuery($_REQUEST['secondactor']);

  $person_1_ID = $person_1_obj["results"][0]["id"];
  $person_2_ID = $person_2_obj["results"][0]["id"];

  $person_1_name = $person_1_obj["results"][0]["name"];
  $person_2_name = $person_2_obj["results"][0]["name"];

  $credits_1 = combinedCreditsQuery($person_1_ID);
  $credits_2 = combinedCreditsQuery($person_2_ID);

  $movie_list = compareCredits($credits_1, $credits_2);

  //var_dump(getGalleryData($movie_list, $person_1_name, $person_2_name));

  $gallery_data = getGalleryData($movie_list, $person_1_name, $person_2_name);
  $spotlight_data = getSpotlightData($person_1_obj, $person_2_obj);

  $m = new Mustache_Engine(array(
      'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/views'),
  ));

  // loads template from `views/hello_world.mustache` and renders it.
  $gallery_html = $m->render('poster_gallery', $gallery_data);
  $spotlight_html = $m->render('spotlight', $spotlight_data);

  $returns = array(
    "gallery" => $gallery_html,
    "spotlight" => $spotlight_html,
  );

  echo json_encode($returns);
} elseif ($_REQUEST["type"] == 0) {

  $autocomplete_data = file_get_contents("autocomplete.txt");
  $autocomplete = json_decode($autocomplete_data, true);


  echo json_encode($autocomplete);
}
//echo $gallery_html;

?>
