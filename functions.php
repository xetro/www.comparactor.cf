<?php

require 'constants.php';

ini_set('display_errors', 1);

function updateConfiguration() {

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $GLOBALS["API_BASE_URL"] . "configuration?api_key=" . $GLOBALS["API_KEY"]);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json"));

  $response = curl_exec($ch);

  curl_close($ch);

  $result = json_decode($response, true);

  $GLOBALS["IMG_BASE_URL"] = $result["images"]["base_url"];

  // For getting a numerical key index.
  $index = 0;

  foreach ($GLOBALS["BACKDROP_SIZE"] as $key => $value) {

    $value = $result["images"]["backdrop_sizes"][$index];
    $GLOBALS["BACKDROP_SIZE"][$key] = $value;
    $index++;
  }

  // For getting a numerical key index.
  $index = 0;

  foreach ($GLOBALS["LOGO_SIZE"] as $key => $value) {

    $value = $result["images"]["logo_sizes"][$index];
    $GLOBALS["LOGO_SIZE"][$key] = $value;
    $index++;
  }

  // For getting a numerical key index.
  $index = 0;

  foreach ($GLOBALS["POSTER_SIZE"] as $key => $value) {

    $value = $result["images"]["poster_sizes"][$index];
    $GLOBALS["POSTER_SIZE"][$key] = $value;
    $index++;
  }

  // For getting a numerical key index.
  $index = 0;

  foreach ($GLOBALS["STILL_SIZE"] as $key => $value) {

    $value = $result["images"]["still_sizes"][$index];
    $GLOBALS["STILL_SIZE"][$key] = $value;
    $index++;
  }
}

function searchPersonQuery($actor) {

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $GLOBALS["API_BASE_URL"] . "search/person?query=" . urlencode($actor) . "&api_key=" . $GLOBALS["API_KEY"]);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json"));

  $response = curl_exec($ch);

  curl_close($ch);

  $result = json_decode($response, true);

  return $result;

}

function combinedCreditsQuery($id) {

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $GLOBALS["API_BASE_URL"] . "person/" . $id ."/combined_credits?api_key=" . $GLOBALS["API_KEY"]);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json"));

  $response = curl_exec($ch);

  curl_close($ch);

  $result = json_decode($response, true);

  return $result;

}

function compareCredits($credits_1, $credits_2) {

  $movie_list = Array();

  foreach ($credits_1["cast"] as $movie_1) {

    foreach ($credits_2["cast"] as $movie_2) {

      if ($movie_1["id"] == $movie_2["id"]) {

        $paired_movie = Array($movie_1, $movie_2);

        array_push($movie_list, $paired_movie);

      }
    }
  }

  return $movie_list;

}

function getGalleryData($movie_list, $name_1, $name_2) {

  $gallery_data = array(
    "movies" => array()
  );

  foreach ($movie_list as $movie) {

    // Check if he was involvend in a movie as an actor
    if ($movie[0]["media_type"] === "movie") {

      $title = $movie[0]["title"];
      if (empty($title)) {
        $title = "Unavailable";
      }

      $date = $movie[0]["release_date"];
      if (empty($date)) {
        $date = "Unavailable";
      }

      $role_1 = $movie[0]["character"];
        if (empty($role_1)) {
          $role_1 = "Unavailable";
        }

      $role_2 = $movie[1]["character"];
        if (empty($role_2)) {
          $role_2 = "Unavailable";
        }

      $poster_url = $GLOBALS["IMG_BASE_URL"] . $GLOBALS["POSTER_SIZE"]["l"] . $movie[0]["poster_path"];
        if (empty($movie[0]["poster_path"])) {
          $poster_url = "images/blank_poster.jpg";
        }

      $poster_data = Array(
        "title" => $title,
        "date" => $date,
        "poster_url" => $poster_url,
        "actor_1" => $name_1,
        "role_1" => $role_1,
        "actor_2" => $name_2,
        "role_2" => $role_2
      );

      array_push($gallery_data["movies"], $poster_data);
    }

  }

  return $gallery_data;

}

function getSpotlightData($person_1, $person_2){

  $actors = array($person_1, $person_2);
  $spotlight_array = array(
    "actors" => array()
  );

  foreach ($actors as $actor) {
    $actor = $actor["results"][0];

    //var_dump($actor);
    $actor_data = array(
      "name" => $actor["name"],
      "rating" => array(),
      "profile" => $GLOBALS["IMG_BASE_URL"] . $GLOBALS["PROFILE_SIZE"]["m"] . $actor["profile_path"],
      "known_for" => array()
    );

    //var_dump($actor["popularity"]);
    $popularity = round(($actor["popularity"]), 0, PHP_ROUND_HALF_UP) / $GLOBALS["POP_FACTOR"];
    //var_dump($pop_factor);

    for ($i=0; $i < $popularity; $i++) {
      array_push($actor_data["rating"], "");

    }

    foreach ($actor["known_for"] as $movie) {

      $title_array = array('title' => $movie["title"]);
      array_push($actor_data["known_for"], $title_array);

    }

    array_push($spotlight_array["actors"], $actor_data);

  }

  return $spotlight_array;

}

?>
