<?php

ini_set('display_errors', 1);

$API_KEY = "46453f9e2429778d74c0334c523e0919";
$API_BASE_URL = "http://api.themoviedb.org/3/";
$IMG_BASE_URL = "http://image.tmdb.org/t/p/";
$MAX_POP = "41.288741";
$POP_FACTOR = round($MAX_POP, 0, PHP_ROUND_HALF_UP) / 10;
$BACKDROP_SIZE = array(

  's' => 'w300',
  'm' => 'w780',
  'l' => 'w1280',
  'orig' => 'original'
  );

$LOGO_SIZE = array(

  'xs' => 'w45',
  's' => 'w92',
  'm' => 'w154',
  'l' => 'w185',
  'xl' => 'w300',
  'xxl' => 'w500',
  'orig' => 'original'
  );

$POSTER_SIZE = array(

  'xs' => 'w92',
  's' => 'w154',
  'm' => 'w185',
  'l' => 'w342',
  'xl' => 'w500',
  'xxl' => 'w780',
  'orig' => 'original'
  );

$PROFILE_SIZE = array(

  's' => 'w45',
  'm' => 'w185',
  'l' => 'h632',
  'orig' => 'original'
  );

$STILL_SIZE = array(

  's' => 'w92',
  'm' => 'w185',
  'l' => 'w300',
  'orig' => 'original'
  );

?>
